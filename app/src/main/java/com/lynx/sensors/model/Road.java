package com.lynx.sensors.model;

/**
 * Created by isdzulqor on 6/29/18.
 */

public class Road {
    private String state;
    private double distance;
    private String timestamp;
    private Gyro gyro;
    private Accelero accelero;
    private LatLong latLong;

    public Road() {}

    public Road(String state, double distance, String timestamp, Gyro gyro, Accelero accelero, LatLong latLong) {
        this.state = state;
        this.distance = distance;
        this.timestamp = timestamp;
        this.gyro = gyro;
        this.accelero = accelero;
        this.latLong = latLong;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Gyro getGyro() {
        return gyro;
    }

    public void setGyro(Gyro gyro) {
        this.gyro = gyro;
    }

    public Accelero getAccelero() {
        return accelero;
    }

    public void setAccelero(Accelero accelero) {
        this.accelero = accelero;
    }

    public LatLong getLatLong() {
        return latLong;
    }

    public void setLatLong(LatLong latLong) {
        this.latLong = latLong;
    }
}
