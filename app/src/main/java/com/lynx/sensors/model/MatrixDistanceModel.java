package com.lynx.sensors.model;

import java.util.List;

/**
 *  on 6/7/16.
 */
public class MatrixDistanceModel {
    private List<String> destination_addresses, origin_addresses;
    private List<Row> rows;
    private String status;

    public MatrixDistanceModel() {
    }

    public MatrixDistanceModel(List<String> destination_addresses, List<String> origin_addresses, List<Row> rows, String status) {
        this.destination_addresses = destination_addresses;
        this.origin_addresses = origin_addresses;
        this.rows = rows;
        this.status = status;
    }

    public List<String> getDestination_addresses() {
        return destination_addresses;
    }

    public void setDestination_addresses(List<String> destination_addresses) {
        this.destination_addresses = destination_addresses;
    }

    public List<String> getOrigin_addresses() {
        return origin_addresses;
    }

    public void setOrigin_addresses(List<String> origin_addresses) {
        this.origin_addresses = origin_addresses;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class Row {
        private List<Element> elements;

        public Row(List<Element> elements) {
            this.elements = elements;
        }

        public Row() {
        }

        public List<Element> getElements() {
            return elements;
        }

        public void setElements(List<Element> elements) {
            this.elements = elements;
        }
    }

    public class Element {
        private Distance distance;
        private Duration duration;
        private String status;

        public Element() {
        }

        public Element(Distance distance, Duration duration, String status) {
            this.distance = distance;
            this.duration = duration;
            this.status = status;
        }

        public Distance getDistance() {
            return distance;
        }

        public void setDistance(Distance distance) {
            this.distance = distance;
        }

        public Duration getDuration() {
            return duration;
        }

        public void setDuration(Duration duration) {
            this.duration = duration;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }


    }
    public class Distance {
        private String text;
        private int value;

        public Distance() {
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public class Duration {
        private String text;
        private int value;

        public Duration() {
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}
