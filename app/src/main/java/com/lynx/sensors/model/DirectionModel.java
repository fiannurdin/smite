package com.lynx.sensors.model;

import java.util.List;

/**
 *  on 6/8/16.
 */
public class DirectionModel {
    private List<GeocodedWaypoints> geocoded_waypoints;
    private List<Route> routes;
    private String status;

    public DirectionModel(List<GeocodedWaypoints> geocoded_waypoints, List<Route> routes, String status) {
        this.geocoded_waypoints = geocoded_waypoints;
        this.routes = routes;
        this.status = status;
    }

    public DirectionModel() {
    }

    public List<GeocodedWaypoints> getGeocoded_waypoints() {
        return geocoded_waypoints;
    }

    public void setGeocoded_waypoints(List<GeocodedWaypoints> geocoded_waypoints) {
        this.geocoded_waypoints = geocoded_waypoints;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class Route {
        private Bound bounds;
        private String copyrights;
        private List<Leg> legs;
        private Polyline overview_polyline;
        private String summary;
        private List<String> warnings;
        private List<String> waypoint_order;

        public Route(Bound bounds, String copyrights, List<Leg> legs, Polyline overview_polyline, String summary, List<String> warnings, List<String> waypoint_order) {
            this.bounds = bounds;
            this.copyrights = copyrights;
            this.legs = legs;
            this.overview_polyline = overview_polyline;
            this.summary = summary;
            this.warnings = warnings;
            this.waypoint_order = waypoint_order;
        }

        public Route() {
        }

        public Bound getBounds() {
            return bounds;
        }

        public void setBounds(Bound bounds) {
            this.bounds = bounds;
        }

        public String getCopyrights() {
            return copyrights;
        }

        public void setCopyrights(String copyrights) {
            this.copyrights = copyrights;
        }

        public List<Leg> getLegs() {
            return legs;
        }

        public void setLegs(List<Leg> legs) {
            this.legs = legs;
        }

        public Polyline getOverview_polyline() {
            return overview_polyline;
        }

        public void setOverview_polyline(Polyline overview_polyline) {
            this.overview_polyline = overview_polyline;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public List<String> getWarnings() {
            return warnings;
        }

        public void setWarnings(List<String> warnings) {
            this.warnings = warnings;
        }

        public List<String> getWaypoint_order() {
            return waypoint_order;
        }

        public void setWaypoint_order(List<String> waypoint_order) {
            this.waypoint_order = waypoint_order;
        }
    }

    public class Leg {
        private MatrixDistanceModel.Distance distance;
        private MatrixDistanceModel.Duration duration;
        private String end_address;
        private LatLngOwn end_location;
        private String start_address;
        private LatLngOwn start_location;
        private List<Step> steps;

        public Leg(MatrixDistanceModel.Distance distance, MatrixDistanceModel.Duration duration, String end_address, LatLngOwn end_location, String start_address, LatLngOwn start_location, List<Step> steps) {
            this.distance = distance;
            this.duration = duration;
            this.end_address = end_address;
            this.end_location = end_location;
            this.start_address = start_address;
            this.start_location = start_location;
            this.steps = steps;
        }

        public Leg() {
        }

        public MatrixDistanceModel.Distance getDistance() {
            return distance;
        }

        public void setDistance(MatrixDistanceModel.Distance distance) {
            this.distance = distance;
        }

        public MatrixDistanceModel.Duration getDuration() {
            return duration;
        }

        public void setDuration(MatrixDistanceModel.Duration duration) {
            this.duration = duration;
        }

        public String getEnd_address() {
            return end_address;
        }

        public void setEnd_address(String end_address) {
            this.end_address = end_address;
        }

        public LatLngOwn getEnd_location() {
            return end_location;
        }

        public void setEnd_location(LatLngOwn end_location) {
            this.end_location = end_location;
        }

        public String getStart_address() {
            return start_address;
        }

        public void setStart_address(String start_address) {
            this.start_address = start_address;
        }

        public LatLngOwn getStart_location() {
            return start_location;
        }

        public void setStart_location(LatLngOwn start_location) {
            this.start_location = start_location;
        }

        public List<Step> getSteps() {
            return steps;
        }

        public void setSteps(List<Step> steps) {
            this.steps = steps;
        }


    }

    public class Step {
        private MatrixDistanceModel.Distance distance;
        private MatrixDistanceModel.Duration duration;
        private LatLngOwn end_location;
        private String html_instructions;
        private Polyline polyline;
        private LatLngOwn start_location;
        private String travel_mode;

        public Step(MatrixDistanceModel.Distance distance, MatrixDistanceModel.Duration duration, LatLngOwn end_location, String html_instructions, Polyline polyline, LatLngOwn start_location, String travel_mode) {
            this.distance = distance;
            this.duration = duration;
            this.end_location = end_location;
            this.html_instructions = html_instructions;
            this.polyline = polyline;
            this.start_location = start_location;
            this.travel_mode = travel_mode;
        }

        public Step() {
        }

        public MatrixDistanceModel.Distance getDistance() {
            return distance;
        }

        public void setDistance(MatrixDistanceModel.Distance distance) {
            this.distance = distance;
        }

        public MatrixDistanceModel.Duration getDuration() {
            return duration;
        }

        public void setDuration(MatrixDistanceModel.Duration duration) {
            this.duration = duration;
        }

        public LatLngOwn getEnd_location() {
            return end_location;
        }

        public void setEnd_location(LatLngOwn end_location) {
            this.end_location = end_location;
        }

        public String getHtml_instructions() {
            return html_instructions;
        }

        public void setHtml_instructions(String html_instructions) {
            this.html_instructions = html_instructions;
        }

        public Polyline getPolyline() {
            return polyline;
        }

        public void setPolyline(Polyline polyline) {
            this.polyline = polyline;
        }

        public LatLngOwn getStart_location() {
            return start_location;
        }

        public void setStart_location(LatLngOwn start_location) {
            this.start_location = start_location;
        }

        public String getTravel_mode() {
            return travel_mode;
        }

        public void setTravel_mode(String travel_mode) {
            this.travel_mode = travel_mode;
        }


    }
    public class Polyline {
        private String points;

        public Polyline() {
        }
        public Polyline(String points) {
            this.points = points;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }
    }
    public class LatLngOwn {
        private double lat;
        private double lng;

        public LatLngOwn() {
        }

        public LatLngOwn(double lat, double lng) {
            this.lat = lat;
            this.lng = lng;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public class Bound {
        private LatLngOwn northeast;
        private LatLngOwn southwest;

        public Bound(LatLngOwn northeast, LatLngOwn southwest) {
            this.northeast = northeast;
            this.southwest = southwest;
        }

        public Bound() {
        }

        public LatLngOwn getNortheast() {
            return northeast;
        }

        public void setNortheast(LatLngOwn northeast) {
            this.northeast = northeast;
        }

        public LatLngOwn getSouthwest() {
            return southwest;
        }

        public void setSouthwest(LatLngOwn southwest) {
            this.southwest = southwest;
        }
    }

    public class GeocodedWaypoints {
        private String geocoder_status;
        private String place_id;
        private List<String> types;

        public GeocodedWaypoints(String geocoder_status, String place_id, List<String> types) {
            this.geocoder_status = geocoder_status;
            this.place_id = place_id;
            this.types = types;
        }

        public GeocodedWaypoints() {
        }

        public String getGeocoder_status() {
            return geocoder_status;
        }

        public void setGeocoder_status(String geocoder_status) {
            this.geocoder_status = geocoder_status;
        }

        public String getPlace_id() {
            return place_id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }
    }
}
