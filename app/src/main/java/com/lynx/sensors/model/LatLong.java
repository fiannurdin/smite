package com.lynx.sensors.model;

/**
 * Created by isdzulqor on 6/29/18.
 */

public class LatLong {
    private double latitude;
    private double longitude;

    public LatLong() {}
    public LatLong(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
