package com.lynx.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

public class Gyroscope extends AppCompatActivity implements SensorEventListener {
    @BindColor(R.color.red)
    int redColor;
    @BindColor(R.color.green)
    int greenColor;
    @BindColor(R.color.blue)
    int blueColor;

    private SensorManager sensorManager;
    private Sensor sensor;
    private long lastUpdate = 0;
    private float last_x, last_y, las_z;
    private final String TAG = getClass().getSimpleName();
    private List<Entry> entriesGyroX = new ArrayList<>(), entriesGyroY = new ArrayList<>(), entriesGyroZ = new ArrayList<>();
    private LineDataSet dataSetGyroX, dataSetGyroY, dataSetGyroZ;

    private int xAxis = 0;
    private Entry entry = null;
    @BindView(R.id.chart_gyroscope)
    LineChart mChart;
    @BindView(R.id.tv_x)
    TextView tvX;
    @BindView(R.id.tv_y)
    TextView tvY;
    @BindView(R.id.tv_z)
    TextView tvZ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gyroscope);

        ButterKnife.bind(this);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        visualize();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        initChart();
    }

    private void initChart() {
        mChart.setBackgroundColor(redColor);
    }

    private void visualize() {
        entriesGyroX.add(new Entry(0, 0));
        entriesGyroY.add(new Entry(0, 0));
        entriesGyroZ.add(new Entry(0, 0));

        dataSetGyroX = new LineDataSet(entriesGyroX, "X");
        dataSetGyroX.setColor(ActivityCompat.getColor(this, R.color.white));
        dataSetGyroX.setValueTextColor(ActivityCompat.getColor(this, R.color.white));
        dataSetGyroX.setDrawValues(false);
        dataSetGyroX.setDrawCircles(false);

        dataSetGyroY = new LineDataSet(entriesGyroY, "Y");
        dataSetGyroY.setColor(greenColor);
        dataSetGyroY.setValueTextColor(ActivityCompat.getColor(this, R.color.white));
        dataSetGyroY.setDrawValues(false);
        dataSetGyroY.setDrawCircles(false);

        dataSetGyroZ = new LineDataSet(entriesGyroZ, "Z");
        dataSetGyroZ.setColor(blueColor);
        dataSetGyroZ.setValueTextColor(ActivityCompat.getColor(this, R.color.white));
        dataSetGyroZ.setDrawValues(false);
        dataSetGyroZ.setDrawCircles(false);

        LineData lineData = new LineData(dataSetGyroX, dataSetGyroY, dataSetGyroZ);
        mChart.setData(lineData);
        mChart.invalidate();
        YAxis yAxis = mChart.getAxisLeft();
//        yAxis.setAxisMaximum((float)10);
//        yAxis.setAxisMinimum((float)-50);
        yAxis.disableAxisLineDashedLine();
        yAxis.disableGridDashedLine();
        mChart.getXAxis().setEnabled(false);
        mChart.getAxisRight().setEnabled(false);
        lastUpdate = System.currentTimeMillis() / 100;
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        long currentMilis = System.currentTimeMillis() / 100;
        if (sensor.getType() == Sensor.TYPE_GYROSCOPE && (currentMilis - lastUpdate >= 1)) {
            LineData data = mChart.getLineData();
            if (data != null) {
                for (int i = 0; i < 3; i++) {
                    ILineDataSet set = data.getDataSetByIndex(i);
                    lastUpdate = currentMilis;
                    entry = new Entry(set.getEntryCount(), event.values[i]);
                    data.addEntry(entry, i);
                    data.notifyDataChanged();
                    mChart.notifyDataSetChanged();
                    mChart.setVisibleXRangeMaximum(25);
                    mChart.moveViewToX(data.getEntryCount());
                }
                tvX.setText(String.valueOf(event.values[0]));
                tvY.setText(String.valueOf(event.values[1]));
                tvZ.setText(String.valueOf(event.values[2]));

//                ILineDataSet set = data.getDataSetByIndex(0);
//                lastUpdate = currentMilis;
//                float x = event.values[0];
//                float y = event.values[1];
//                float z = event.values[2];
//                float speed = (x + y + z);
//                Log.d(TAG, "speed : " + speed);
//                entry = new Entry(set.getEntryCount(), speed);
//                data.addEntry(entry,0);
//                data.notifyDataChanged();
//                mChart.notifyDataSetChanged();
//                mChart.setVisibleXRangeMaximum(25);
//                mChart.moveViewToX(data.getEntryCount());
                xAxis++;
            }
        }
        Log.d(TAG, "current time : " + currentMilis);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
