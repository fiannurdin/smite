package com.lynx.sensors;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lynx.sensors.model.Accelero;
import com.lynx.sensors.model.DirectionModel;
import com.lynx.sensors.model.Gyro;
import com.lynx.sensors.model.LatLong;
import com.lynx.sensors.model.Road;
import com.lynx.sensors.service.ApiService;
import com.lynx.sensors.service.MqttHelper;
import com.lynx.sensors.service.PahoMqttClient;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity implements SensorEventListener, LocationListener, OnMapReadyCallback {
    @BindString(R.string.google_maps_key)
    String API_MAP_KEY;

    @BindColor(R.color.red)
    int redColor;
    @BindColor(R.color.orange)
    int orangeColor;
    @BindColor(R.color.black)
    int blackColor;
    @BindColor(R.color.white)
    int whiteColor;

    @BindView(R.id.tv_x_gyro)
    TextView tvXGyro;
    @BindView(R.id.tv_y_gyro)
    TextView tvYGyro;
    @BindView(R.id.tv_z_gyro)
    TextView tvZGyro;

    @BindView(R.id.tv_x_accelero)
    TextView tvXAccelero;
    @BindView(R.id.tv_y_accelero)
    TextView tvYAccelero;
    @BindView(R.id.tv_z_accelero)
    TextView tvZAccelero;

    @BindView(R.id.tv_longitude)
    TextView tvLongitude;
    @BindView(R.id.tv_latitude)
    TextView tvLatitude;

    @BindView(R.id.publish)
    Button btStart;

    private Location LOCATION_GLOBAL = new Location("dummyprovider");

    private int TIMER = 0;
    private int TIMER_BEF = 0;


    private Road ROAD_TEMP = new Road();
    private boolean IS_ROAD = false;
    private boolean IS_STOP = false;
    private boolean IS_START = false;
    private LatLong START_LAT_LONG;
    Gson gson = new GsonBuilder().create();
    private GoogleMap mMap;
    private Boolean mLocationPermissionsGranted = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private static final float DEFAULT_ZOOM = 15f;

    //map
    SupportMapFragment mapFragment;
    //

    private SensorManager sensorManager;
    private Sensor accelerometerSensor, gyroSensor;

    private long lastUpdate = 0;

    private final String TAG = "Geolocation";
    protected Double _longitude;
    protected Double _latitude;

    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;


    private MqttAndroidClient client;
    private PahoMqttClient pahoMqttClient;

    private boolean MQTT_CONNECTED = false;


    MqttHelper mqttHelper;


    //chart init accelero
    private List<Entry> entriesAcceleroX = new ArrayList<>(), entriesAcceleroY = new ArrayList<>(), entriesAcceleroZ = new ArrayList<>();
    private LineDataSet dataSetAcceleroX, dataSetAcceleroY, dataSetAcceleroZ;
    private int xAxisAccelero = 0;
    private Entry entryAccelero = null;
    @BindView(R.id.chart_accelerometer)
    LineChart chartAcclerometer;
    //

    //chart init gyro
    private float last_xGyro, last_yGyro, las_zGyro;
    private List<Entry> entriesGyroX = new ArrayList<>(), entriesGyroY = new ArrayList<>(), entriesGyroZ = new ArrayList<>();
    private LineDataSet dataSetGyroX, dataSetGyroY, dataSetGyroZ;
    private int xAxisGyro = 0;
    private Entry entryGyro = null;
    @BindView(R.id.chart_gyroscope)
    LineChart mChartGyro;

    //

    private String messageComplete = "", messageGyro = "", messageAccelero = "";
    private String messageLocation = "\n" +
            "\t\"location\": {\n" +
            "\t\t\"lat\": " + 0 + ",\n" +
            "\t\t\"long\": " + 0 + "\n" +
            "\t}\n" +
            "";;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        if (!Util.isInternetAvailable(getApplicationContext())) {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btStart.getText().toString().equalsIgnoreCase("START")){
                    btStart.setText("STOP");
                    ROAD_TEMP.setState("START");

                    IS_ROAD = true;
                    IS_STOP = false;
                    IS_START = true;
                } else {
                    btStart.setText("START");
                    ROAD_TEMP.setState("STOP");
                    IS_STOP = true;
                    IS_START = false;
                }
            }
        });

        //map
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //


        startMqtt();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        lastUpdate = System.currentTimeMillis() / 100;

        visualizeAccelero();
        visualizeGyro();


        sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);

        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    Log.d(TAG, "Permission requests");
                    canGetLocation = false;
                }
            }

            // get location
            try {
                getLocation();
            } catch (MqttException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        initChart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor sensor = sensorEvent.sensor;

        long currentMilis = System.currentTimeMillis() / 100;
        if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            float x = (float) Util.round(sensorEvent.values[0], 2);
            float y = (float) Util.round(sensorEvent.values[1], 2);
            float z = (float) Util.round(sensorEvent.values[2], 2);

            float speed = (x + y + z);

//            Toast.makeText(this, "halo bos", Toast.LENGTH_SHORT).show();
//            lastUpdate = currentMilis;this
            tvXGyro.setText(String.valueOf(x));
//            tvXGyro.setText(String.valueOf(currentMilis));
            tvYGyro.setText(String.valueOf(y));
            tvZGyro.setText(String.valueOf(z));

            messageGyro = "\n" +
                    "\t\"gyro\": {\n" +
                    "\t\t\"x\": " + String.valueOf(x) + ",\n" +
                    "\t\t\"y\": " + String.valueOf(y) + ",\n" +
                    "\t\t\"z\": " + String.valueOf(z) + "\n" +
                    "\t}\n" +
                    "";

//            Toast.makeText(this, "cc" + message, Toast.LENGTH_SHORT).show();

//            if (MQTT_CONNECTED) {
//                try {
//                    publishMessage(mqttHelper.mqttAndroidClient, messageGyro, 1, Constants.PUBLISH_TOPIC_GYRO);
//                } catch (MqttException e) {
//                    e.printStackTrace();
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//            }

            LineData data = chartAcclerometer.getLineData();
            if (data != null) {
                for (int i = 0; i < 3; i++) {
                    ILineDataSet set = data.getDataSetByIndex(i);
                    lastUpdate = currentMilis;
                    Log.d(TAG, "speed index " + i + " : " + speed);
                    entryAccelero = new Entry(set.getEntryCount(), sensorEvent.values[i]);
                    data.addEntry(entryAccelero, i);
                    data.notifyDataChanged();
                    chartAcclerometer.notifyDataSetChanged();
                    chartAcclerometer.setVisibleXRangeMaximum(25);
                    chartAcclerometer.moveViewToX(data.getEntryCount());
                }
                xAxisAccelero++;
            }

            if(IS_ROAD){
//                getLastLocation();

                Log.e("COK | accelero ", "isROAD");
                Accelero acceleroTemp = new Accelero(x, y, z);
                if(ROAD_TEMP.getAccelero() == null){
                    ROAD_TEMP.setAccelero(acceleroTemp);
                }

                if(ROAD_TEMP.getGyro() != null && ROAD_TEMP.getAccelero() != null && ROAD_TEMP.getLatLong() != null){
                    preprocessData();
                }
            }

        } else if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = (float) Util.round(sensorEvent.values[0], 2);
            float y = (float) Util.round(sensorEvent.values[1], 2);
            float z = (float) Util.round(sensorEvent.values[2],2);
            float speed = (x + y + z);

            lastUpdate = currentMilis;
//
            tvXAccelero.setText(String.valueOf(x));
//            tvXGyro.setText(String.valueOf(currentMilis));
            tvYAccelero.setText(String.valueOf(y));
            tvZAccelero.setText(String.valueOf(z));

            messageAccelero = "\n" +
                    "\t\"accelero\": {\n" +
                    "\t\t\"x\": " + String.valueOf(x) + ",\n" +
                    "\t\t\"y\": " + String.valueOf(y) + ",\n" +
                    "\t\t\"z\": " + String.valueOf(z) + "\n" +
                    "\t}\n" +
                    "";

//            if (MQTT_CONNECTED) {
//                try {
//                    publishMessage(mqttHelper.mqttAndroidClient, messageAccelero, 1, Constants.PUBLISH_TOPIC_ACCELERO);
//                } catch (MqttException e) {
//                    e.printStackTrace();
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//            }

            LineData data = mChartGyro.getLineData();
            if (data != null) {
                for (int i = 0; i < 3; i++) {
                    ILineDataSet set = data.getDataSetByIndex(i);
                    lastUpdate = currentMilis;
                    entryGyro = new Entry(set.getEntryCount(), sensorEvent.values[i]);
                    data.addEntry(entryGyro, i);
                    data.notifyDataChanged();
                    mChartGyro.notifyDataSetChanged();
                    mChartGyro.setVisibleXRangeMaximum(25);
                    mChartGyro.moveViewToX(data.getEntryCount());
                }
                xAxisGyro++;
            }

            if(IS_ROAD){
//                getLastLocation();

                Log.e("COK | gyro ", "isROAD");
                Gyro gyroTemp = new Gyro(x, y, z);
                if(ROAD_TEMP.getGyro() == null){
                    ROAD_TEMP.setGyro(gyroTemp);
                }
                if(ROAD_TEMP.getGyro() != null && ROAD_TEMP.getAccelero() != null && ROAD_TEMP.getLatLong() != null){
                    preprocessData();
                }
            }
        }

        messageLocation = "\n" +
                "\t\"location\": {\n" +
                "\t\t\"lat\": " + Double.toString(LOCATION_GLOBAL.getLatitude()) + ",\n" +
                "\t\t\"long\": " + Double.toString(LOCATION_GLOBAL.getLongitude()) + "\n" +
                "\t}\n" +
                "";
        messageComplete = "{" + messageGyro + "," + messageAccelero + "," + messageLocation + "}";

        if (MQTT_CONNECTED) {
            try {
                publishMessage(mqttHelper.mqttAndroidClient, messageComplete, 1, Constants.PUBLISH_TOPIC_MIX);
            } catch (MqttException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        Log.d("TAG", "current time : " + currentMilis);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    @Override
    public void onLocationChanged(final Location location) {
        if(IS_ROAD){
            Log.e("COK | onLocationChanged", "isROAD");
            LatLong latLongTemp = new LatLong();
            latLongTemp.setLatitude(location.getLatitude());
            latLongTemp.setLongitude(location.getLongitude());

            if(ROAD_TEMP.getLatLong() == null){
                ROAD_TEMP.setLatLong(latLongTemp);
            }

            if(ROAD_TEMP.getGyro() != null && ROAD_TEMP.getAccelero() != null && ROAD_TEMP.getLatLong() != null){
                preprocessData();
            }
        }

        if(IS_START){
            START_LAT_LONG = new LatLong(location.getLatitude(), location.getLongitude());
            IS_START = false;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    updateUI(location);
                    LOCATION_GLOBAL = location;
                } catch (MqttException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        try {
            getLocation();
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLocation() throws MqttException, UnsupportedEncodingException {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else {
                    loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        Log.e("cok | getLastLocation", "getLastLocation");

        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            final Location location = locationManager.getLastKnownLocation(provider);
            LOCATION_GLOBAL = location;
            Log.e("cok", "getLastLocation location : "+location);

            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
//                    Toast.makeText(HomeActivity.this, "jancok", Toast.LENGTH_SHORT).show();
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(location.getLatitude(), location.getLongitude()), 10));

                }
            });

//            if(IS_ROAD){
//                Log.e("COK | onLocationChanged", "isROAD");
//                LatLong latLongTemp = new LatLong();
//                latLongTemp.setLatitude(location.getLatitude());
//                latLongTemp.setLongitude(location.getLongitude());
//
//                if(ROAD_TEMP.getLatLong() == null){
//                    ROAD_TEMP.setLatLong(latLongTemp);
//                }
//
//                if(ROAD_TEMP.getGyro() != null && ROAD_TEMP.getAccelero() != null && ROAD_TEMP.getLatLong() != null){
//                    preprocessData();
//                }
//            }
//
//            if(IS_START){
//                START_LAT_LONG = new LatLong(location.getLatitude(), location.getLongitude());
//                IS_START = false;
//            }


            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    try {
                        getLocation();
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(HomeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(Location loc) throws MqttException, UnsupportedEncodingException {
        Log.d(TAG, "updateUI");
        tvLatitude.setText(Double.toString(loc.getLatitude()));
        tvLongitude.setText(Double.toString(loc.getLongitude()));
        messageLocation = "\n" +
                "\t\"location\": {\n" +
                "\t\t\"lat\": " + Double.toString(loc.getLatitude()) + ",\n" +
                "\t\t\"long\": " + Double.toString(loc.getLongitude()) + "\n" +
                "\t}\n" +
                "";
        messageComplete = "{" + messageAccelero + "," + messageGyro + "," + messageLocation + "}";
        if (MQTT_CONNECTED) {
            try {
                Log.e("COK TOPIC_MIX", "Constants.PUBLISH_TOPIC_MIX\nmessage : "+messageComplete);
                Toast.makeText(HomeActivity.this, "message : "+messageComplete, Toast.LENGTH_SHORT).show();
//                 publishMessage(mqttHelper.mqttAndroidClient, messageLocation, 1, Constants.PUBLISH_TOPIC_LOCATION);
                publishMessage(mqttHelper.mqttAndroidClient, messageComplete, 1, Constants.PUBLISH_TOPIC_MIX);
            } catch (MqttException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void initMqtt() {
        pahoMqttClient = new PahoMqttClient();
        client = pahoMqttClient.getMqttClient(HomeActivity.this, Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);
    }

    private void startMqtt() {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                MQTT_CONNECTED = true;

                String msg = "asds";
                Log.w("Debug", "Connected");

                try {
                    publishMessage(mqttHelper.mqttAndroidClient, msg, 1, Constants.PUBLISH_TOPIC);
                } catch (MqttException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void connectionLost(Throwable throwable) {
                MQTT_CONNECTED = false;
                connectMqtt(mqttHelper.mqttAndroidClient);
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
        connectMqtt(mqttHelper.mqttAndroidClient);
    }

    private void publishMessage(@NonNull MqttAndroidClient client, @NonNull String msg, int qos, @NonNull String topic)
            throws MqttException, UnsupportedEncodingException {
        byte[] encodedPayload = new byte[0];
        encodedPayload = msg.getBytes("UTF-8");
        MqttMessage message = new MqttMessage(encodedPayload);
        message.setId(320);
        message.setRetained(true);
        message.setQos(qos);
        client.publish(topic, message);
    }

    private void connectMqtt(final MqttAndroidClient client) {
        try {
            client.connect(getApplicationContext(), new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    MQTT_CONNECTED = true;
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    MQTT_CONNECTED = false;
                    System.out.println("Connection Failure!");
                    Log.w("MQTT BOS", "Connection Failure!");
                }
            });
        } catch (MqttException ex) {

        }
    }

    private void visualizeAccelero() {
        entriesAcceleroX.add(new Entry(0, 0));
        entriesAcceleroY.add(new Entry(0, 0));
        entriesAcceleroZ.add(new Entry(0, 0));
        dataSetAcceleroX = new LineDataSet(entriesAcceleroX, "X");
        dataSetAcceleroX.setColor(ActivityCompat.getColor(this, R.color.white));
        dataSetAcceleroX.setValueTextColor(ActivityCompat.getColor(this, R.color.white));
        dataSetAcceleroX.setDrawValues(false);
        dataSetAcceleroX.setDrawCircles(false);

        dataSetAcceleroY = new LineDataSet(entriesAcceleroY, "Y");
        dataSetAcceleroY.setColor(ActivityCompat.getColor(this, R.color.green));
        dataSetAcceleroY.setValueTextColor(ActivityCompat.getColor(this, R.color.green));
        dataSetAcceleroY.setDrawValues(false);
        dataSetAcceleroY.setDrawCircles(false);

        dataSetAcceleroZ = new LineDataSet(entriesAcceleroZ, "Z");
        dataSetAcceleroZ.setColor(ActivityCompat.getColor(this, R.color.colorPrimary));
        dataSetAcceleroZ.setValueTextColor(ActivityCompat.getColor(this, R.color.colorPrimary));
        dataSetAcceleroZ.setDrawValues(false);
        dataSetAcceleroZ.setDrawCircles(false);

        LineData lineData = new LineData(dataSetAcceleroX, dataSetAcceleroY, dataSetAcceleroZ);
        chartAcclerometer.setData(lineData);
        chartAcclerometer.invalidate();
        YAxis yAxis = chartAcclerometer.getAxisLeft();
        yAxis.setAxisMaximum((float) 60);
        yAxis.setAxisMinimum((float) -50);
        yAxis.disableAxisLineDashedLine();
        yAxis.disableGridDashedLine();
        chartAcclerometer.getXAxis().setEnabled(false);
        chartAcclerometer.getAxisRight().setEnabled(false);
        lastUpdate = System.currentTimeMillis() / 100;
    }

    private void visualizeGyro() {
        entriesGyroX.add(new Entry(0, 0));
        entriesGyroY.add(new Entry(0, 0));
        entriesGyroZ.add(new Entry(0, 0));

        dataSetGyroX = new LineDataSet(entriesGyroX, "X");
        dataSetGyroX.setColor(ActivityCompat.getColor(this, R.color.white));
        dataSetGyroX.setValueTextColor(ActivityCompat.getColor(this, R.color.white));
        dataSetGyroX.setDrawValues(false);
        dataSetGyroX.setDrawCircles(false);

        dataSetGyroY = new LineDataSet(entriesGyroY, "Y");
        dataSetGyroY.setColor(ActivityCompat.getColor(this, R.color.green));
        dataSetGyroY.setValueTextColor(ActivityCompat.getColor(this, R.color.green));
        dataSetGyroY.setDrawValues(false);
        dataSetGyroY.setDrawCircles(false);

        dataSetGyroZ = new LineDataSet(entriesGyroZ, "Z");
        dataSetGyroZ.setColor(ActivityCompat.getColor(this, R.color.blue));
        dataSetGyroZ.setValueTextColor(ActivityCompat.getColor(this, R.color.blue));
        dataSetGyroZ.setDrawValues(false);
        dataSetGyroZ.setDrawCircles(false);

        LineData lineData = new LineData(dataSetGyroX, dataSetAcceleroY, dataSetAcceleroZ);
        mChartGyro.setData(lineData);
        mChartGyro.invalidate();
        YAxis yAxis = mChartGyro.getAxisLeft();
//        yAxis.setAxisMaximum((float)10);
//        yAxis.setAxisMinimum((float)-50);
        yAxis.disableAxisLineDashedLine();
        yAxis.disableGridDashedLine();
        mChartGyro.getXAxis().setEnabled(false);
        mChartGyro.getAxisRight().setEnabled(false);
        lastUpdate = System.currentTimeMillis() / 100;
    }

    private void initChart() {
        chartAcclerometer.setBackgroundColor(redColor);
        mChartGyro.setBackgroundColor(orangeColor);
    }


    float randomWithRange(float min, float max) {
        float range = Math.abs(max - min);
        return (float) ((Math.random() * range) + (min <= max ? min : max));
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(37.4233438, -122.0728817), 10));
        mMap = googleMap;
//        Location myLocation = googleMap.getMyLocation();

        PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(this,
                new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION},
                new PermissionsResultAction() {
                    @Override
                    public void onGranted() {
                        mLocationPermissionsGranted = true;
                        mMap.setMyLocationEnabled(true);
                        getDeviceLocation();
                    }

                    @Override
                    public void onDenied(String permission) {
                        Log.e(TAG, "Unable to get location without permission");
                    }
                });


//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(37.4233438, -122.0728817), 10));
    }

    private void getDeviceLocation(){
        Log.d(TAG, "getDeviceLocation: getting the devices current location");
//        Toast.makeText(MainActivity.this, "function getDeviceLocation()", Toast.LENGTH_SHORT).show();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try{
            if(mLocationPermissionsGranted){

                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "onComplete: found location!");
                            Location currentLocation = (Location) task.getResult();


                            LatLng latLng= new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
//                            Toast.makeText(MainActivity.this, "latLngFrom isi", Toast.LENGTH_SHORT).show();

                            moveCamera(latLng,
                                    DEFAULT_ZOOM,
                                    "My Location");
//                            Toast.makeText(MainActivity.this, "current location", Toast.LENGTH_SHORT).show();

                        }else{
                            Log.d(TAG, "onComplete: current location is null");
//                            Toast.makeText(MainActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Log.d(TAG, "getDeviceLocation: getting the devices current location");
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage() );
        }
    }


    private void getDirection() {
        if(IS_STOP){
            if (MQTT_CONNECTED) {
                try {
                    String messageTemp = gson.toJson(ROAD_TEMP);
                    Log.e("COK | getDirection", "publishMessage : "+messageTemp);
                    publishMessage(mqttHelper.mqttAndroidClient, messageTemp, 1, Constants.PUBLISH_TOPIC_ROAD);
                } catch (MqttException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            IS_STOP = false;
            IS_ROAD = false;
            ROAD_TEMP = new Road();
        }
    }
    private void getDirection(String from, final String destination) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiService.API_MAP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService.ApiMap service = retrofit
                .create(ApiService.ApiMap.class);

        Call<DirectionModel> listCall = service.getDirectionMap(
                from,
                destination,
                API_MAP_KEY
        );

        listCall.enqueue(new Callback<DirectionModel>() {
            @Override
            public void onResponse(Call<DirectionModel> call, Response<DirectionModel> response) {
                DirectionModel directionModel = response.body();
                DirectionModel.Route route = directionModel.getRoutes().get(0);
                DirectionModel.Leg leg = route.getLegs().get(0);
                float distanceValue = leg.getDistance().getValue();

                String distanceText = leg.getDistance().getText();
                String durationTemp = leg.getDuration().getText();

                ROAD_TEMP.setTimestamp(new Date().toGMTString());
                ROAD_TEMP.setDistance(distanceValue);
                Log.e("FIN | getDirection", "distance : "+distanceValue);

                if(IS_STOP){
                    if (MQTT_CONNECTED) {
                        try {
                            String messageTemp = gson.toJson(ROAD_TEMP);
                            Log.e("FIN | getDirection", "publishMessage : "+messageTemp);
                            publishMessage(mqttHelper.mqttAndroidClient, messageTemp, 1, Constants.PUBLISH_TOPIC_ROAD);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    IS_STOP = false;
                    IS_ROAD = false;
                    ROAD_TEMP = new Road();
                }
            }

            @Override
            public void onFailure(Call<DirectionModel> call, Throwable t) {

            }
        });
    }

    private void preprocessData(){
        if(TIMER > TIMER_BEF){
            Log.e("FIN | preprocessData", "pre");
            if(IS_STOP){
                Log.e("FIN | preprocessData", "IS_STOP");

                String fromTemp = START_LAT_LONG.getLatitude()+","+START_LAT_LONG.getLongitude();
                String destTemp = ROAD_TEMP.getLatLong().getLatitude()+","+ROAD_TEMP.getLatLong().getLongitude();

                getDirection();
//                getDirection(fromTemp, destTemp);
            } else {
                Log.e("FIN | preprocessData", "!IS_STOP");
                ROAD_TEMP.setTimestamp(new Date().toGMTString());
                if (MQTT_CONNECTED) {
                    try {
                        String messageTemp = gson.toJson(ROAD_TEMP);
                        Log.e("FIN | preprocessData", "publishMessage : "+messageTemp);
                        publishMessage(mqttHelper.mqttAndroidClient, messageTemp, 1, Constants.PUBLISH_TOPIC_ROAD);

                        if(ROAD_TEMP.getState().equalsIgnoreCase("START")){
                            ROAD_TEMP.setState("PROGRESS");
                        }
                    } catch (MqttException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }

            TIMER_BEF = TIMER;
        }

    }

    private void moveCamera(LatLng latLng, float zoom, String title){
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude );
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        if(!title.equals("My Location")){
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title(title);
            mMap.addMarker(options);
        }

        hideSoftKeyboard();
    }

    private void hideSoftKeyboard(){
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void callTimer(){
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                TIMER++;
            }
        }, 0, 5000);
    }
}

