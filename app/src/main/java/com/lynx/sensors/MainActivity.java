package com.lynx.sensors;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @OnClick(R.id.buttonAccelerometer) void onAccClick(){
        startActivity(new Intent(this,AccelerometerActivity.class));
    }

    @OnClick(R.id.buttonGyroscope) void onGyroClick(){
        startActivity(new Intent(this,Gyroscope.class));
    }

    @OnClick(R.id.buttonData) void onDataClick(){
        startActivity(new Intent(this,HomeActivity.class));
    }

    @OnClick(R.id.buttonAbout) void onAboutClick(){
        startActivity(new Intent(this,AboutActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if(!Util.isInternetAvailable(getApplicationContext())){
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}
