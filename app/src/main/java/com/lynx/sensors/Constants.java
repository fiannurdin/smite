package com.lynx.sensors;

/**
 * Created by brijesh on 20/4/17.
 */

public class Constants {

    public static final String MQTT_BROKER_URL = "tcp://202.182.58.12:4001";

    public static final String PUBLISH_TOPIC = "sensor";
    public static final String PUBLISH_TOPIC_LOCATION = "sensor/location";
    public static final String PUBLISH_TOPIC_GYRO = "sensor/gyro";
    public static final String PUBLISH_TOPIC_ACCELERO = "sensor/accelero";

    public static final String PUBLISH_TOPIC_MIX = "sensor/all";

    public static final String CLIENT_ID = "androidkta";

    public static final String PUBLISH_TOPIC_ROAD = "road";

}

