package com.lynx.sensors.service;

import com.lynx.sensors.model.DirectionModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 *  on 5/26/18.
 */

public class ApiService {
    public static final String THINGSPEAK_API_KEY= "24Y0F1RQTDVCOCLY";
    public static final String API_MAP_URL = "https://maps.googleapis.com/maps/api/";
    public static final String API_THINGSPEAK = "https://api.thingspeak.com/channels/";

    public interface ApiMap {
        @GET("directions/json")
        Call<DirectionModel> getDirectionMap(
                @Query("origin") String origin,
                @Query("destination") String destination,
                @Query("key") String key
        );
    }


}
