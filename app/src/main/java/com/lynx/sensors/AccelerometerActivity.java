package com.lynx.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AccelerometerActivity extends AppCompatActivity implements SensorEventListener {
    @BindColor(R.color.red)
    int redColor;
    @BindColor(R.color.green)
    int greenColor;
    @BindColor(R.color.blue)
    int blueColor;

    private SensorManager sensorManager;
    private Sensor sensor;
    private long lastUpdate = 0;
    private float last_x, last_y, las_z;
    private final String TAG = AccelerometerActivity.class.getSimpleName();
    private List<Entry> entriesAcceleroX = new ArrayList<>(), entriesAcceleroY = new ArrayList<>(), entriesAcceleroZ = new ArrayList<>();
    private LineDataSet dataSetAcceleroX, dataSetAcceleroY, dataSetAcceleroZ;

    private int xAxis = 0;
    private Entry entry = null;
    @BindView(R.id.chart_accelerometer) LineChart chartAcclerometer;
    @BindView(R.id.tv_x)
    TextView tvX;
    @BindView(R.id.tv_y)
    TextView tvY;
    @BindView(R.id.tv_z)
    TextView tvZ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer);
        ButterKnife.bind(this);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        visualize();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        initChart();
    }

    private void initChart() {
        chartAcclerometer.setBackgroundColor(blueColor);
    }

    private void visualize() {
        entriesAcceleroX.add(new Entry(0, 0));
        entriesAcceleroY.add(new Entry(0, 0));
        entriesAcceleroZ.add(new Entry(0, 0));

        dataSetAcceleroX = new LineDataSet(entriesAcceleroX, "X");
        dataSetAcceleroX.setColor(ActivityCompat.getColor(this, R.color.white));
        dataSetAcceleroX.setValueTextColor(ActivityCompat.getColor(this, R.color.white));
        dataSetAcceleroX.setDrawValues(false);
        dataSetAcceleroX.setDrawCircles(false);

        dataSetAcceleroY = new LineDataSet(entriesAcceleroY, "Y");
        dataSetAcceleroY.setColor(greenColor);
        dataSetAcceleroY.setValueTextColor(ActivityCompat.getColor(this, R.color.white));
        dataSetAcceleroY.setDrawValues(false);
        dataSetAcceleroY.setDrawCircles(false);

        dataSetAcceleroZ = new LineDataSet(entriesAcceleroZ, "Z");
        dataSetAcceleroZ.setColor(redColor);
        dataSetAcceleroZ.setValueTextColor(ActivityCompat.getColor(this, R.color.white));
        dataSetAcceleroZ.setDrawValues(false);
        dataSetAcceleroZ.setDrawCircles(false);

        LineData lineData = new LineData(dataSetAcceleroX, dataSetAcceleroY, dataSetAcceleroZ);

        chartAcclerometer.setData(lineData);
        chartAcclerometer.invalidate();
        YAxis yAxis = chartAcclerometer.getAxisLeft();
        yAxis.setAxisMaximum((float)60);
        yAxis.setAxisMinimum((float)-50);
        yAxis.disableAxisLineDashedLine();
        yAxis.disableGridDashedLine();
        chartAcclerometer.getXAxis().setEnabled(false);
        chartAcclerometer.getAxisRight().setEnabled(false);
        lastUpdate = System.currentTimeMillis()/100;
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        long currentMilis = System.currentTimeMillis()/100;
        if(sensor.getType() == Sensor.TYPE_ACCELEROMETER && (currentMilis - lastUpdate >= 1)){
            LineData data = chartAcclerometer.getLineData();
            if(data!=null) {
                for (int i = 0; i < 3; i++) {
                    ILineDataSet set = data.getDataSetByIndex(i);
                    lastUpdate = currentMilis;
                    entry = new Entry(set.getEntryCount(), event.values[i]);
                    data.addEntry(entry, i);
                    data.notifyDataChanged();
                    chartAcclerometer.notifyDataSetChanged();
                    chartAcclerometer.setVisibleXRangeMaximum(25);
                    chartAcclerometer.moveViewToX(data.getEntryCount());
                }
                tvX.setText(String.valueOf(event.values[0]));
                tvY.setText(String.valueOf(event.values[1]));
                tvZ.setText(String.valueOf(event.values[2]));

//                ILineDataSet set = data.getDataSetByIndex(0);
//                lastUpdate = currentMilis;
//                float x = event.values[0];
//                float y = event.values[1];
//                float z = event.values[2];
//                float speed = (x + y + z);
//                Log.d(TAG, "speed : " + speed);
//                entry = new Entry(set.getEntryCount(), speed);
//                data.addEntry(entry,0);
//                data.notifyDataChanged();
//                chartAcclerometer.notifyDataSetChanged();
//                chartAcclerometer.setVisibleXRangeMaximum(25);
//                chartAcclerometer.moveViewToX(data.getEntryCount());
                xAxis++;
            }
        }
        Log.d(TAG,"current time : " + currentMilis);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
